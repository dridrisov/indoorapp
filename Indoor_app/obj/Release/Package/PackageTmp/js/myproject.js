var n=0, kol=0, sch = 0, del = 0, num_from, num_to, n_real=n;
var mass = new Array(n);
var floor = -1;
var floor_i;
var massLevel = new Array(n);
var massName = new Array(n_real);
var currentLevel;
var etag = 10;
var dots = new Array(n);
var cursorX;
var cursorY;
document.onmousemove = function(e){
    cursorX = e.pageX;
    cursorY = e.pageY;
}
function PrintMatrix(){

	$("#Matrix").empty();
	var table="<table>";
	for ( var i = 0; i < Object.keys(mass).length + 1; i++ ){
		if (i == 0 && mass.length > 0){
			table += "<tr>";
			for ( var j = 0; j < mass[i].length + 1; j++ ){
				if (j == 0) table += "<th></th>";
				else {
					//alert (j);
					//alert (dots[0]);
					table += "<th>" + dots[j-1].number + "_" + dots[j-1].floor_of +"</th>";
				}
			}
			table += "</tr>";
		} else if (mass.length > 0) { 
			table += "<tr>";
			for ( var j = 0; j < mass[i-1].length + 1; j++){
				if (j == 0){
					table += "<th>" + dots[i-1].number + "_" + dots[i-1].floor_of +"</th>";
				} else {
					table += (mass[i-1][j-1] ? "<td class='connected'>" : "<td>") + mass[i-1][j-1] +"</td>";
				}
			}
			table += "</tr>";
		}
	}
		
	table+="</table>";
	$('#Matrix').append(table);
}




function rebuildMatrix(){
	if (kol >=1 ){
			var mass2 = new Array(n_real);
			for(var i = 0; i < n_real; i++)
				mass2[i] = new Array(n_real);
			for(var i = 0; i < n_real; i++)
				for(var j=0; j < n_real; j++)
					mass2[i][j]=0;
				
			for(var i = 0; i < Object.keys(mass).length; i++)
				for(var j=0; j < mass[i].length; j++){
					mass2[i][j]=mass[i][j];
				}
					
			for(var i = 0; i < n_real; i++)
				mass[i] = new Array(n_real);
				
			for(var i = 0; i < Object.keys(mass).length; i++)
				for(var j=0; j < mass[i].length; j++){
					mass[i][j]=mass2[i][j];
				}
		} else {

			for(var i = 0; i < n_real; i++)
				mass[i] = new Array(n_real);

			for(var i = 0; i < n_real; i++)
				for(var j=0; j < n_real; j++)
					mass[i][j]=0;
			
		}
}

function DelFromMatrix(d){
	//alert(n_real+" "+d);
		var mass2 = new Array(n_real);
		for(var i = 0; i < n_real; i++)
			mass2[i] = new Array(n_real);
	

		for(var i = 0; i < n_real; i++)
			for(var j = 0; j < n_real; j++)
				mass2[i][j]=0;

		for(var i = 0; i < d; i++)
			for(var j = 0; j < d; j++){
				mass2[i][j]=mass[i][j];
			}

		for(var i = d + 1; i < n_real+1; i++)
			for(var j = 0; j < d; j++){
				mass2[i - 1][j]=mass[i][j];
			}

		for(var i = 0; i < d; i++)
			for(var j = d + 1; j < n_real+1; j++){
				//alert(i+" "+j)
				mass2[i][j - 1]=mass[i][j];
			}

		for(var i = d + 1; i < n_real+1; i++)
			for(var j = d + 1; j < n_real+1; j++){
				mass2[i - 1][j - 1]=mass[i][j];
			}
		/*for(var i = 0; i < Object.keys(mass).length; i++)
			alert("mass1 "+mass[i]);
		for(var i = 0; i < Object.keys(mass2).length; i++)
			alert("mass2 "+mass2[i]);*/
		return mass2;
		mass = mass2.splice(0);
}


function PrintInform(){

	$("#Inform").empty();
	var table="<table>";
	for ( var i = 0; i < n_real + 1; i++ ){
		if (i == 0){
			table += "<tr>";
			for ( var j = 0; j < 6 + 1; j++ ){
				switch(j){
					case 0: 
						table += "<th></th>";
						break;
					case 1:
						table += "<th>" + 'Number' + "</th>"
						break;
					case 2:
						table += "<th>" + 'RNumber' + "</th>"
						break;
					case 3:
						table += "<th>" + 'X' + "</th>"
						break;
					case 4:
						table += "<th>" + 'Y' + "</th>"
						break;
					case 5:
						table += "<th>" + 'Floor' + "</th>"
						break;
					case 6:
						table += "<th>" + 'Class' + "</th>"
						break;
				}
			}
			table += "</tr>";
		} else {
			table += "<tr>";
			for ( var j = 0; j < 6 + 1; j++){
				switch(j){
					case 0: 
						table += "<th>" + i +"</th>";
						break;
					case 1:
						table += (dots[i-1].number ? "<td class='connected'>" : "<td>") + dots[i-1].number +"</td>";
						break;
					case 2:
						table += (dots[i-1].real_number ? "<td class='connected'>" : "<td>") + dots[i-1].real_number +"</td>";
						break;
					case 3:
						table += (dots[i-1].X ? "<td class='connected'>" : "<td>") + dots[i-1].X +"</td>";
						break;
					case 4:
						table += (dots[i-1].Y ? "<td class='connected'>" : "<td>") + dots[i-1].Y +"</td>";
						break;
					case 5:
						table += (dots[i-1].floor_of ? "<td class='connected'>" : "<td>") + dots[i-1].floor_of +"</td>";
						break;
					case 6:
						table += (dots[i-1].class_ ? "<td class='connected'>" : "<td>") + dots[i-1].class_ +"</td>";
						break;
				}

			}
			table += "</tr>";
		}
	}
	table+="</table>";
	$('#Inform').append(table);
}


function level(i){
	var to_hide;
	/*var img_src = 'url(img/strogino/level';//maps/MIEM/ + i +/ + i +end
	var end = '.png)'
	img_src = img_src + i;
	img_src = img_src + end;

	var img_src_1 = 'img/strogino/level'
	var end_1 = '.png'
	img_src_1 = img_src_1 + i;
	img_src_1 = img_src_1 + end_1;*/
	var img_src = 'url(maps/MIEM/';//maps/MIEM/ + i +/ + i +end
	var end = '.png)'
	var sl = '/'
	img_src = img_src + i;
	img_src = img_src + sl;
	img_src = img_src + i;
	img_src = img_src + end;

	var img_src_1 = 'maps/MIEM/'
	var end_1 = '.png'
	img_src_1 = img_src_1 + i;
	img_src_1 = img_src_1 + sl;
	img_src_1 = img_src_1 + i;
	img_src_1 = img_src_1 + end_1;
	//document.body.style.backgroundImage = img_src;


	var img = new Image();
	img.onload = function() {
		p = $('#Box');
		p.css("width", this.width);
		p.css("height", this.height);
	};
	img.src = img_src_1;


	document.getElementById('Box').style.backgroundImage = img_src;


	for (var h = 0; h < etag; h++){
		activebtn = document.getElementById("button" + h);
		if (h != i && activebtn){
			activebtn.style.backgroundColor = "lightgray";
		}
		else if (activebtn){
			activebtn.style.backgroundColor = "lightgreen";
			currentLevel = h;
		}
		
	}

	if (floor >= 0){
		for (var l = 0; l < n_real; l++ ){
			if (dots[l].floor_of == floor){
				m = dots[l].number;
				var to_hide = "number" + m;
				jsPlumb.selectEndpoints({source:to_hide}).setVisible(false);
				$("div#number"+m).css("display","none");
			}
		}
	}

	floor = i;
	for (var l = 0; l < n_real; l++ ){
		if (dots[l].floor_of == i){
			m = dots[l].number;
			var to_see = "number" + m;
			jsPlumb.selectEndpoints({source:to_see}).setVisible(true);
			$("div#number"+m).css("display","flex");
			$("div#number"+m).css("display","flex");
		}
	}

	var a = $('.chbox');
	a.each(function (index, elem) {
		elem.checked = true;
	})

}
function level_JSON(i){
	var to_hide;

	for (var h = 0; h < etag; h++){
		activebtn = document.getElementById("button" + h);
		if (h != i && activebtn) {
			activebtn.style.backgroundColor = "lightgray";
		}
		else if (activebtn){
			activebtn.style.backgroundColor = "lightgreen";
		}
		
	}

	if (floor >= 0){
		for (var l = 0; l < n_real; l++ ){
			if (dots[l].floor_of == floor){
				m = dots[l].number;
				var to_hide = "number" + m;
				jsPlumb.selectEndpoints({source:to_hide}).setVisible(false);
				$("div#number"+m).css("display","none");
			}
		}
	}

	floor = i;
	for (var l = 0; l < n_real; l++ ){
		if (dots[l].floor_of == i){
			m = dots[l].number;
			var to_see = "number" + m;
			jsPlumb.selectEndpoints({source:to_see}).setVisible(true);
			$("div#number"+m).css("display","flex");
		}
	}

}
function showPrompt(n, callback) {
	sch = 1;
	kol++;
	var coverDiv = document.createElement('div');
	coverDiv.id = 'cover-div';
	document.body.appendChild(coverDiv);
	var form = document.getElementById('form');
	var container = document.getElementById('dialog');
	$("#form").empty();
	var down = n - 1;
	var up = down + 2;
	var str = "<input type='radio' name='r' value=";
	if (up > 9){
		str += down + '>' + down + '<br>';
	}
	else{
		if (down < 0){
			str += up + '>' + up + '<br>';
		}
		else{
			str += down + '>' + down + '<br>';
			str += "<input type='radio' name='r' value=";
			str += up + '>' + up + '<br>';
		}
	}
	$('#form').append(str);
}


function dot(number, real_number, X, Y, floor_of, class_) {
	this.number = number;
	this.real_number = real_number;
  	this.X = X;
  	this.Y = Y;
  	this.floor_of = floor_of;
  	this.class_ = class_;
  	this.text = number;
}

jsPlumb.ready(function() {
	var common1 = {
        isSource:true,
        isTarget:true,
		endpoint:["Dot",{radius:10}],
		connector: ["Straight"],
		paintStyle:{outlineColor:"tomato", fillStyle:"lightcoral", lineWidth:1, outlineWidth:1 },
		connectorStyle:{ strokeStyle:"tomato", lineWidth:2 },
		connectorHoverStyle:{ lineWidth:3 },
		maxConnections: n_real-1,
		uuid: "number"+ (n)
     };

     var common2 = {
        isSource:true,
        isTarget:true,
		endpoint:["Dot",{radius:10}],
		connector: ["Straight"],
		paintStyle:{outlineColor:"blue", fillStyle:"blue", lineWidth:1, outlineWidth:1 },
		connectorStyle:{ strokeStyle:"tomato", lineWidth:2 },
		connectorHoverStyle:{ lineWidth:3 },
		maxConnections: n_real-1,
		uuid: "number"+ (n)
     };

     var common3 = {
        isSource:true,
        isTarget:true,
		endpoint:["Dot",{radius:10}],
		connector: ["Straight"],
		paintStyle:{outlineColor:"green", fillStyle:"green", lineWidth:1, outlineWidth:1 },
		connectorStyle:{ strokeStyle:"tomato", lineWidth:2 },
		connectorHoverStyle:{ lineWidth:3 },
		maxConnections: n_real-1,
		uuid: "number"+ (n)
     };
     var common4 = {//dmrdrsv
        isSource:true,
        isTarget:true,
		endpoint:["Dot",{radius:10}],
		connector: ["Straight"],
		paintStyle:{outlineColor:"pink", fillStyle:"pink", lineWidth:1, outlineWidth:1 },
		connectorStyle:{ strokeStyle:"tomato", lineWidth:2 },
		connectorHoverStyle:{ lineWidth:3 },
		maxConnections: n_real-1,
		uuid: "number"+ (n)
     };

    $('#dialog').dialog({
		modal: true,
		autoOpen: false,
		buttons: [{text: "OK", click: function() {
		    fl_to = $('input[name=r]:checked').val();

		    level(fl_to);
			$(this).dialog("close");
		}}]		
	});	

     $.contextMenu({
            selector: '.context-menu-one', 
            callback: function(key, options) {
            	
                //var m = "clicked: " + key;
                var id = options.$trigger.attr("id");
                var class_ = options.$trigger.attr("class");
                //alert(key + ' ' + id + ' ' + class_);

                if (key == "delete")
                {
                	//alert(class_);
					if ( class_.indexOf("tran") > 0){
						//alert(class_.indexOf("tran"));
						del++;
						n_real--;
						var d = parseInt(id.replace(/\D+/g,""));

						for (var i = 0; i < n_real + 1; i++){
							if (dots[i].number == d){
								var dd = dots[i].real_number - 1;
								break;
							}
						}
						
						for (var i = 0; i < n_real + 1; i++){

							if ((mass[i][dd] == 1) && (Math.abs(dd - i) == 1) && (dots[i].class_=="tran")){
								var with_d = i;
							}
						}

						var min_d = Math.min(dd, with_d);
						var max_d = Math.max(dd, with_d);
						
						dots.splice(max_d, 1);

						for (var i = max_d; i < n_real; i++){
							var nn = dots[i].real_number;
							nn--;
							dots[i].real_number = nn;
						}

						PrintInform();
						mass = DelFromMatrix(max_d);
						PrintMatrix();

						jsPlumb.removeAllEndpoints(id);
						jsPlumb.selectEndpoints({source:id}).detachAll();
						$("#number"+d).remove();


						
						del++;
						n_real--;


						dots.splice(min_d, 1);
						for (var i = min_d; i < n_real; i++){
							var nn = dots[i].real_number;
							nn--;
							dots[i].real_number = nn;
						}
						PrintInform();
						mass = DelFromMatrix(min_d);
						PrintMatrix();

						
						if (dd == min_d){
							d++;
						} else {
							d--;
						}
						var id2 = "number" + d;

						jsPlumb.removeAllEndpoints(id2);
						jsPlumb.selectEndpoints({source:id2}).detachAll();
						$("#number"+d).remove();
				
					}
					else{
						del++;
						n_real--;
						var d = parseInt(id.replace(/\D+/g,""));
						for (var i = 0; i < n_real + 1; i++){
							if (dots[i].number == d) {
								var dd = dots.indexOf(dots[i])
								break;
							}
						}
						dots.splice(dd, 1);
						for (var i = dd; i < n_real; i++){
							var nn = dots[i].real_number;
							nn--;
							dots[i].real_number = nn;
						}
						PrintInform();
						mass = DelFromMatrix(dd);
						
						PrintMatrix();
						//jsPlumb.remove(id);

						jsPlumb.removeAllEndpoints(id);
						jsPlumb.selectEndpoints({source:id}).detachAll();
						$("#number" + d).remove();
					}

					var id = parseInt(id.replace(/\D+/g, ""));
					
					$.ajax({
						type: 'POST',
						url: '/api/deleteNode/' + id,
						success: function (response) {
							console.log(response);

						}
					});

					var id = $("#BuildingId").val()
					data = {
						"matrix": JSON.stringify(mass),
					};

					$.ajax({
						type: 'POST',
						url: '/api/updateMatrix/' + id,
						data: JSON.stringify(data),
						dataType: 'json',
						contentType: "application/json; charset=UTF-8",
						success: function (response) {
							console.log(response);//возвращается всегда id - новый или обновленной записи

						}
					});
                }

                if(key == "edit")
                {
                	var d = parseInt(id.replace(/\D+/g,""));
                	var dot = dots.find(function(el){
                		return el.number === d;
                	})
                	var panel = $(".edit_panel");
                	panel.css('display', 'flex');                	             	
                	panel.css('left', cursorX);
                	panel.css('top', cursorY);

					var description = $('#edit_panel_description');
					description.val(dot.description);

					var roomNum = $('#edit_panel_roomNum');
					roomNum.val(dot.roomNum);

					var roomType = $('#edit_panel_roomType');
					roomType.val(dot.roomType);

                	$('#edit_save').on('click', function(e){
						panel.css('display', 'none');
						var id = $("#BuildingId").val()

						var node = {
							description: description.val(),
							roomNum: roomNum.val(),
							roomType: roomType.val(),
							coordX: dot.X,
							coordY: dot.Y,
							type: dot.class_,
							buildingID: id,
							mapLevel: parseInt(dot.floor_of),
							wifiSpot: null,
							nodeID: dot.nodeID
						}

						$.ajax({
							type: 'POST',
							url: '/api/updateNode/' + dot.nodeID,
							data: JSON.stringify(node),
							dataType: 'json',
							contentType: "application/json; charset=UTF-8",
							success: function (response) {
								console.log(response);
							}
						});
						dot.description = description.val();
						dot.roomNum = roomNum.val();
						dot.roomType = roomType.val();						
                		$('#edit_save').unbind();
                	})
                	$('#edit_close').on('click', function(e){
                		panel.css('display', 'none');
                		$('#edit_close').unbind();
					})					
               	}

            //window.console && console.log(m) || alert(m); 
                
            },
            items: {
                "edit": {name: "Edit", icon: "edit"},
                "delete": {name: "Delete", icon: "delete"},
            }
        });

        //$('.context-menu-one').on('click', function(e){
            //console.log('clicked', this);
        //});
	 
	 

	$("#Box").click(function (e) {
		if (floor >=0){
		    if ($("#chkAdd").is(':checked'))
		    {
				var common;
				var class_;
				n++;
				n_real++;

				if ($("#rbIB").is(':checked'))
			    {
			    	common = common1;
			    	class_ = "ibeacon";
			    }

			    if ($("#rbNode").is(':checked'))
			    {
			    	common = common2;
			    	class_ = "node";
			    }


			    if ($("#rbTran").is(':checked'))
			    {
			    	common = common3;
			    	class_ = "tran";
			    	if (sch == 0){
	      				showPrompt(floor, function(value) {});
	      				$('#dialog').dialog("open");
	      				num_from = n;
			    	}
	      			else
	      			{
	      				num_to = n;
	      				sch = 0;
	      			}
			    }

			    if ($("#rbWiFi").is(':checked'))//dmrdrsv
			    {
			    	common = common4;
			    	class_ = "Wi-Fi";
			    }

			    $('<div class="object"></div>')
				.css("top", (e.pageY - 18 - $(this).offset().top)).css("left", (e.pageX - 36 - $(this).offset().left))
				.attr('id', 'number'+n)
				.addClass(class_)
				.addClass("context-menu-one")
				.text(n)
				.appendTo('#Box');

				var width = `${Math.max(30, n.length * 15)}px`;

				$(`#number${n}`).css("width", width);
				
				//alert($(".object").outerHeight(true));

				jsPlumb.addEndpoint('number'+n, { 
			        	anchor: [1, 0.5, 1, 0]
			    		}, common);

			    if ($("#rbTran").is(':checked') && (sch ==0))
			    {
			    	var k = n - 1;
			    	for (var i = 1; i < n_real + 1; i++){
						if (dots[i-1].number == k){
							var k_fl = dots[i-1].floor_of;
							break;
						}
					}
			    	$("#number"+k).attr('title', k_fl + "->" + fl_to);
					$("#number"+n).attr('title', fl_to + "->" + k_fl);
				}


				var newDot = new dot(n, n_real, Math.round(e.pageX - $(this).offset().left), Math.round(e.pageY - $(this).offset().top), floor, class_)
				dots[n_real - 1] = newDot;

			    rebuildMatrix();


			    if ($("#rbTran").is(':checked') && (sch == 0)){
				    var num_fromN, num_toN;
					for (var i = 0; i < n_real; i++){
						if (dots[i].number == num_from)
							num_fromN = dots[i].real_number - 1;
					}
					for (var i = 0; i < n_real; i++){
						if (dots[i].number == num_to)
							num_toN = dots[i].real_number - 1;
					}
	      			mass[num_fromN][num_toN] = 1;
	      			mass[num_toN][num_fromN] = 1;
				}

				var id = $("#BuildingId").val()

				var data = {
					description: newDot.text,
					roomNum: 1,
					roomType: null,
					coordX: newDot.X,
					coordY: newDot.Y,
					type: newDot.class_,
					buildingID: id,
					mapLevel: parseInt(newDot.floor_of),
					wifiSpot: null
				}

				$.ajax({
					type: 'POST',
					url: '/api/addnode',
					data: JSON.stringify(data),
					dataType: 'json',
					contentType: "application/json; charset=UTF-8",
					success: function (response) {
						console.log(response);
					}
				});


				PrintMatrix();

				var id = $("#BuildingId").val()
				data = {
					"matrix": JSON.stringify(mass),
				};

				$.ajax({
					type: 'POST',
					url: '/api/updateMatrix/' + id,
					data: JSON.stringify(data),
					dataType: 'json',
					contentType: "application/json; charset=UTF-8",
					success: function (response) {
						console.log(response);//возвращается всегда id - новый или обновленной записи

					}
				});

			    //alert('number'+':'+dots[n-1].number+'\n'+'X'+':'+dots[n-1].X+'\n'+'Y'+':'+dots[n-1].Y+'\n'+'floor'+':'+dots[n-1].floor_of);

			    PrintInform();

			} else{

				$( init );

				function init() {
				  $("div[id^='number']").draggable( {
					  cursor:"move",
					  containment: 'Box',
					  stop: function ( event, ui ) {
						  var id = $(this).attr('id');
						  var nid = parseInt(id.replace(/\D+/g,""));
						  for (i = 0; i < n_real; i++){
						  	if (dots[i].number==nid)
						  		id_r = i;
						  }
						  var c = document.getElementById(id).style;
						  var Top = parseInt(c.top);
						  var Left = parseInt(c.left);
						  dots[id_r].X = Left + 36;
						  dots[id_r].Y = Top + 18;
						  PrintInform();

						  var dot = dots[id_r];

						  var id = $("#BuildingId").val()

						  var node = {
							  description: dot.text,
							  roomNum: 1,
							  roomType: null,
							  coordX: dot.X,
							  coordY: dot.Y,
							  type: dot.class_,
							  buildingID: id,
							  mapLevel: parseInt(dot.floor_of),
							  wifiSpot: null,
							  nodeID: nid
						  }

						  $.ajax({
							  type: 'PUT',
							  url: '/api/updateNode/' + nid,
							  data: JSON.stringify(node),
							  dataType: 'json',
							  contentType: "application/json; charset=UTF-8",
							  success: function (response) {
								  console.log(response);
							  }
						  });

						}
				  });
				  jsPlumb.draggable($("div[id^='number']"), {
					  containment: 'Box'
				  });

				}
			}

		}
    });

    var selectedNodes = ['ibeacon', 'node', 'tran', 'Wi-Fi'];

    $('.chbox').change(function() {
        		if(this.checked) {
            		selectedNodes.push(this.value)
        		}
        		else{
        			selectedNodes.splice(selectedNodes.findIndex(x=>x === this.value), 1)
        		}
				var nodes = dots.filter(x => {
						return selectedNodes.some(y => y === x.class_) && x.floor_of == currentLevel
            	})
        		var difference = dots.filter(x => !nodes.includes(x));
        		nodes.forEach(x=> {
        			var dot = $(`#number${x.number}`);        			
        			dot.css('display', 'flex');
        			var plumb = dot.next();
        			plumb.css('display', 'block');
					var connection = $('.jsplumb-connector').each(function () {
						if (this.attributes.source) {
							if (this.attributes.source.value === `number${x.number}` || this.attributes.target.value === `number${x.number}`) {
								this.style.display = 'block'
							}
						}
        				
        			});
        		})
        		difference.forEach(x=> {
        			var dot = $(`#number${x.number}`);
        			dot.css('display', 'none');
        			var plumb = dot.next();
        			plumb.css('display', 'none');
					var connection = $('.jsplumb-connector').each(function () {
						if (this.attributes.source) {
							if (this.attributes.source.value === `number${x.number}` || this.attributes.target.value === `number${x.number}`) {
								this.style.display = 'none'
							}
						}
        			});
        		})
    		});


	
	jsPlumb.bind('connectionDragStop', function (connection) {
		kol++;
		var SourceN, TargetN;
		var Source=connection.sourceId;
		var Target=connection.targetId;
		connection.canvas.setAttribute('source', Source)
		connection.canvas.setAttribute('target', Target)
		var nSource = parseInt(Source.replace(/\D+/g,""));
		var nTarget = parseInt(Target.replace(/\D+/g,""));
		
		for (var i = 0; i < n_real; i++){
			if (dots[i].number == nSource)
				SourceN = dots.indexOf(dots[i])
		}
		for (var i = 0; i < n_real; i++){
			if (dots[i].number == nTarget)
				TargetN = dots.indexOf(dots[i])
		}

		
		rebuildMatrix();

		mass[SourceN][TargetN]=1;
		mass[TargetN][SourceN]=1;
		
		PrintMatrix();

		var id = $("#BuildingId").val()
		data = {
			"matrix": JSON.stringify(mass),
		};

		$.ajax({
			type: 'POST',
			url: '/api/updateMatrix/' + id,
			data: JSON.stringify(data),
			dataType: 'json',
			contentType: "application/json; charset=UTF-8",
			success: function (response) {
				console.log(response);//возвращается всегда id - новый или обновленной записи

			}
		});
		
	});
	
	jsPlumb.bind('click', function (connection, e) {
		jsPlumb.detach(connection);
		kol--;

		var SourceN, TargetN;
		var Source=connection.sourceId;
		var Target=connection.targetId;
		var nSource = parseInt(Source.replace(/\D+/g,""));
		var nTarget = parseInt(Target.replace(/\D+/g,""));
		
		for (var i = 0; i < n_real; i++){
			if (dots[i].number == nSource)
				SourceN = dots.indexOf(dots[i]);
		}
		for (var i = 0; i < n_real; i++){
			if (dots[i].number == nTarget)
				TargetN = dots.indexOf(dots[i]);
		}

		
		rebuildMatrix();

		mass[SourceN][TargetN] = 0;
		mass[TargetN][SourceN] = 0;
		
		PrintMatrix();

		var id = $("#BuildingId").val()
		data = {
			"matrix": JSON.stringify(mass),
		};

		$.ajax({
			type: 'POST',
			url: '/api/updateMatrix/' + id,
			data: JSON.stringify(data),
			dataType: 'json',
			contentType: "application/json; charset=UTF-8",
			success: function (response) {
				console.log(response);//возвращается всегда id - новый или обновленной записи

			}
		});

	});
	

    $("#btnGet").on("click", function () {
		var id = $("#BuildingId").val()

		$.getJSON("/api/allNodesByBuilding/" + id, function (data) {
			list = data.Nodes;
			matrix = data.Matrix;
			max_number = list.length;			
			
			jsPlumb.deleteEveryEndpoint();
			for (var i = 0; i < n_real + 1; i++) {
				$("#number" + i).remove();
			}

			mass = JSON.parse(matrix);
			dots = [];
			list.forEach(x => {
				var dot = {
					number: x.nodeID,
					real_number: x.nodeID,
					description: x.description,
					nodeID: x.nodeID,
					roomNum: x.roomNum,
					roomType: x.roomType,
					X: x.coordX,
					Y: x.coordY,
					floor_of: x.mapLevel,
					class_: x.type
				}
				dots.push(dot)
			})
			n = max_number;
			n_real = dots.length;
			kol = 0;

			PrintInform();

			for (var i = 1; i <= n_real; i++) {
				level_JSON(dots[i - 1].floor_of);
				var numm = dots[i - 1].number;
				$('<div class="object"></div>')
					.css("left", (dots[i - 1].X - 36)).css("top", (dots[i - 1].Y - 18)).css("display", "flex")
					.attr('id', 'number' + numm)
					.addClass(dots[i - 1].class_)
					.addClass("context-menu-one")
					.text(dots[i - 1].number)
					.appendTo('#Box');

				var width = `${Math.max(30, dots[i - 1].number.toString().length * 15)}px`;

				$(`#number${numm}`).css("width", width);

				if (dots[i - 1].class_ == "beacon" || dots[i - 1].class_ == "ibeacon") {
					common = common1;
				}
				if (dots[i - 1].class_ == "node") {
					common = common2;
				}
				if (dots[i - 1].class_ == "tran") {
					common = common3;
				}
				if (dots[i - 1].class_ == "Wi-Fi") {//dmrdrsv
					common = common4;
				}

				jsPlumb.addEndpoint('number' + numm, {
					anchor: [1, 0.5, 1, 0],
					uuid: "number" + (i)
				}, common);

				if (dots[i - 1].class_ == "tran") {
					for (var g = 1; g < n_real + 1; g++) {
						if (mass[i - 1][g - 1] == 1 && dots[g - 1].class_ == "tran")
							var k_tr = g;
					}
					for (var g = 1; g < n_real + 1; g++) {
						if (dots[g - 1].real_number == k_tr) {
							var k_fl = dots[g - 1].floor_of;
							break;
						}
					}
					for (var g = 1; g < n_real + 1; g++) {
						if (dots[g - 1].real_number == i) {
							var i_fl = dots[g - 1].floor_of;
							break;
						}
					}
					$("#number" + numm).attr('title', i_fl + "->" + k_fl);
				}
			}
			for (var i = 0; i < n_real; i++) {
				for (var j = i; j < n_real; j++) {
					if (mass[i][j] == 1 && (dots[i].class_ != 'tran' || dots[j].class_ != 'tran')) {
						level_JSON(dots[i].floor_of);
						kol += 1;
						var num = 'number'
						var ii = i + 1;
						var jj = j + 1;
						var Source = num + ii;
						var Target = num + jj;
						var jpc = jsPlumb.connect({
							uuids: [Source, Target],
						}, common);
						var canvas = jpc.canvas;
						canvas.setAttribute('source', num + dots[i].number)
						canvas.setAttribute('target', num + dots[j].number)
					}
				}
			}
			level(currentLevel);
			PrintMatrix();
		});
        
    });

    $("#btnPost").on("click", function () {
		var id = $("#BuildingId").val()
		data = {
			"matrix": JSON.stringify(mass),
		};

		$.ajax({
			type: 'POST',
			url: '/api/updateMatrix/' + id,
			data: JSON.stringify(data),
			dataType: 'json',
			contentType: "application/json; charset=UTF-8",
			success: function (response) {
				console.log(response);//возвращается всегда id - новый или обновленной записи

			}
		});
	});





});




 



	
