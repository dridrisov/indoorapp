﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Indoor_app.Models;

namespace Indoor_app.Controllers
{
    [Authorize(Users = AccessLevelUsers.Users)]
    public class floorsController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: floors
        [AllowAnonymous]
        public ActionResult Index(string sortOrder)
        {
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.LevelSortParm = sortOrder == "Level" ? "level_desc" : "Level";
            ViewBag.BuildingSortParm = sortOrder == "building" ? "building_desc" : "building";


            var floors = from s in db.floors select s;
            switch (sortOrder)
            {
                case "Level":
                    floors = floors.OrderBy(s => s.mapLevel);
                    break;
                case "level_desc":
                    floors = floors.OrderByDescending(s => s.mapLevel);
                    break;
                case "building":
                    floors = floors.OrderBy(s => s.buildings.buildingName);
                    break;
                case "building_desc":
                    floors = floors.OrderByDescending(s => s.buildings.buildingName);
                    break;
                case "id_desc":
                    floors = floors.OrderByDescending(s => s.floorID);
                    break;
                default:
                    floors = floors.OrderBy(s => s.floorID);
                    break;
            }
            return View(floors.ToList());
        }

        // GET: floors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            floors floors = db.floors.Find(id);
            if (floors == null)
            {
                return HttpNotFound();
            }
            return View(floors);
        }

        // GET: floors/Create
        public ActionResult Create()
        {
            ViewBag.buildingID = new SelectList(db.buildings, "buildingID", "buildingName");
            return View();
        }

        // POST: floors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "floorID,description,directory,mapLevel,width,height,buildingID, imageFilePath")] floors floor)
        {
            if (ModelState.IsValid)
            {
                //old code
                /*buildings building = db.buildings.Where(b => b.buildingID == floor.buildingID).SingleOrDefault();
                
                string fileName = floor.mapLevel.ToString();
                string extension = Path.GetExtension(floor.imageFilePath.FileName);
                fileName = fileName + extension;

                string directoryName = building.buildingName + "\\";
                directoryName = Path.Combine(Server.MapPath("~/maps/"), directoryName);

                floor.directory = "~/maps/" + building.buildingName + "/" + fileName;

                fileName = Path.Combine(directoryName, fileName);

                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                floor.imageFilePath.SaveAs(fileName);
                db.floors.Add(floor);
                db.SaveChanges();

                return RedirectToAction("Index");*/

                buildings building = db.buildings.Where(b => b.buildingID == floor.buildingID).SingleOrDefault();

                //имя не порезанного файла карты
                string fileName = floor.mapLevel.ToString();
                string extension = Path.GetExtension(floor.imageFilePath.FileName);
                fileName = fileName + extension;

                //пути до картинки
                string directoryName = building.buildingName + "\\" + floor.mapLevel.ToString() + "\\";
                directoryName = Path.Combine(Server.MapPath("~/maps/"), directoryName);

                floor.directory = "~/maps/" + building.buildingName + "/" + floor.mapLevel.ToString() + "\\";

                fileName = Path.Combine(directoryName, fileName);

                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                floor.imageFilePath.SaveAs(fileName);
                db.floors.Add(floor);
                db.SaveChanges();

                var img = Image.FromFile(fileName);
                int map_def_size = 256;

                int xSize = ((int)Math.Ceiling((double)img.Width / map_def_size)); //* map_def_size;
                int ySize = ((int)Math.Ceiling((double)img.Height / map_def_size)); //* map_def_size;

                Size size = new Size(xSize, ySize);

                int tile = 256;

                for (int x = 0; x < size.Width; x++)
                {
                    for (int y = 0; y < size.Height; y++)
                    {
                        string outputFileName = Path.Combine(directoryName, string.Format("{0}-{1}.png", x, y));

                        Rectangle tileBounds = new Rectangle(x * tile, y * tile, tile, tile);
                        Bitmap target = new Bitmap(tile, tile);

                        using (Graphics graphics = Graphics.FromImage(target))
                        {
                            graphics.DrawImage(
                                img,
                                new Rectangle(0, 0, tile, tile),
                                tileBounds,
                                GraphicsUnit.Pixel);
                        }

                        target.Save(outputFileName, ImageFormat.Png);
                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.buildingID = new SelectList(db.buildings, "buildingID", "buildingName", floor.buildingID);
            return View(floor);
        }

        // GET: floors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            floors floors = db.floors.Find(id);
            if (floors == null)
            {
                return HttpNotFound();
            }
            ViewBag.buildingID = new SelectList(db.buildings, "buildingID", "buildingName", floors.buildingID);
            return View(floors);
        }

        // POST: floors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "floorID,description,directory,mapLevel,width,height,buildingID,imageFilePath")] floors floor)
        {
            if (ModelState.IsValid)
            {
                buildings building = db.buildings.Where(b => b.buildingID == floor.buildingID).SingleOrDefault();

                floors oldFloor = db.floors.AsNoTracking().Where(b => b.floorID == floor.floorID).SingleOrDefault();
                int oldMapLevel = oldFloor.mapLevel ?? 0;

                string oldFileName = building.buildingName + "/" + oldFloor.mapLevel.ToString() + Path.GetExtension(oldFloor.directory);
                string oldDirectoryName = Path.Combine(Server.MapPath("~/maps/"), oldFileName);

                db.Entry(floor).State = EntityState.Modified;

                //сначала проверяем загружали ли новую фотографию
                //если да, то удаляем старую и обновляем поле новой записью
                //если нет, то написано
                if (floor.imageFilePath != null)
                {
                    oldFileName = Path.Combine(Server.MapPath("~/maps/"), oldFileName);

                    if (System.IO.File.Exists(oldFileName))
                    {
                        System.IO.File.Delete(oldFileName);
                    }

                    string newFileName = floor.mapLevel.ToString() + Path.GetExtension(oldFloor.directory);

                    floor.directory = "~/maps/" + building.buildingName + "/" + newFileName;

                    newFileName = building.buildingName + "/" + newFileName;
                    newFileName = Path.Combine(Server.MapPath("~/maps/"), newFileName);
                    //newFileName = Path.Combine(oldDirectoryName, newFileName);

                    floor.imageFilePath.SaveAs(newFileName);
                }
                else
                {
                    //нужно чтобы когда не меняем картинку не записывалась пустое значение
                    //другой варик настроить для POST передачу старых данных картинки
                    db.Entry(floor).Property(x => x.directory).IsModified = false;
                }

                if (oldFloor.mapLevel != floor.mapLevel)
                {
                    if (floor.mapLevel != null && oldFloor.mapLevel != null)
                    {
                        string oldFile = building.buildingName + "/" + oldFloor.mapLevel + Path.GetExtension(oldFloor.directory);
                        string newFile = building.buildingName + "/" + floor.mapLevel + Path.GetExtension(oldFloor.directory);

                        floor.directory = "~/maps/" + building.buildingName + "/" + floor.mapLevel.ToString() + Path.GetExtension(oldFloor.directory);

                        oldFile = Path.Combine(Server.MapPath("~/maps/"), oldFile);
                        newFile = Path.Combine(Server.MapPath("~/maps/"), newFile);

                        if (!System.IO.File.Exists(newFile))
                        {
                            System.IO.File.Move(oldFile, newFile);
                        }

                        db.SaveChanges();
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.buildingID = new SelectList(db.buildings, "buildingID", "buildingName", floor.buildingID);
            return View(floor);
        }

        // GET: floors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            floors floors = db.floors.Find(id);
            if (floors == null)
            {
                return HttpNotFound();
            }
            return View(floors);
        }

        // POST: floors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            floors floor = db.floors.Find(id);
            buildings building = db.buildings.Where(b => b.buildingID == floor.buildingID).SingleOrDefault();

            string fileName = building.buildingName + "/" + floor.mapLevel.ToString(); // + Path.GetExtension(floor.directory);
            fileName = Path.Combine(Server.MapPath("~/maps/"), fileName);

            db.floors.Remove(floor);
            db.SaveChanges();

            if (Directory.Exists(fileName))
            {
                Directory.Delete(fileName, true);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
