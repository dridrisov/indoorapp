﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Indoor_app.Models;

namespace Indoor_app.Controllers
{
    [Authorize(Users = AccessLevelUsers.Users)]
    public class wifisController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: wifis
        [AllowAnonymous]
        public ActionResult Index(string sortOrder)
        {
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.PowerSortParm = sortOrder == "power" ? "power_desc" : "power";
            ViewBag.NodeIdSortParm = sortOrder == "nodeID" ? "nodeID_desc" : "nodeID";
            ViewBag.MapLevelSortParm = sortOrder == "mapLevel" ? "mapLevel_desc" : "mapLevel";
            ViewBag.BuildingSortParm = sortOrder == "building" ? "building_desc" : "building";

            var wifiSpots = from s in db.wifi select s;
            switch (sortOrder)
            {
                case "power":
                    wifiSpots = wifiSpots.OrderBy(s => s.power);
                    break;
                case "power_desc":
                    wifiSpots = wifiSpots.OrderByDescending(s => s.power);
                    break;
                case "nodeID":
                    wifiSpots = wifiSpots.OrderBy(s => s.nodes.nodeID);
                    break;
                case "nodeID_desc":
                    wifiSpots = wifiSpots.OrderByDescending(s => s.nodes.nodeID);
                    break;
                case "mapLevel":
                    wifiSpots = wifiSpots.OrderBy(s => s.nodes.floors.mapLevel);
                    break;
                case "mapLevel_desc":
                    wifiSpots = wifiSpots.OrderByDescending(s => s.nodes.floors.mapLevel);
                    break;
                case "building":
                    wifiSpots = wifiSpots.OrderBy(s => s.nodes.floors.buildings.buildingName);
                    break;
                case "building_desc":
                    wifiSpots = wifiSpots.OrderByDescending(s => s.nodes.floors.buildings.buildingName);
                    break;
                case "id_desc":
                    wifiSpots = wifiSpots.OrderByDescending(s => s.wifiID);
                    break;
                default:
                    wifiSpots = wifiSpots.OrderBy(s => s.wifiID);
                    break;
            }
            return View(wifiSpots.ToList());
            //var wifi = db.wifi.Include(w => w.nodes);
            //return View(wifi.ToList());
        }

        // GET: wifis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wifi wifi = db.wifi.Find(id);
            if (wifi == null)
            {
                return HttpNotFound();
            }
            return View(wifi);
        }

        // GET: wifis/Create
        public ActionResult Create()
        {
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID");
            return View();
        }

        // POST: wifis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "wifiID,IP,SSID,power,nodeID")] wifi wifi)
        {
            if (ModelState.IsValid)
            {
                db.wifi.Add(wifi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", wifi.nodeID);
            return View(wifi);
        }

        // GET: wifis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wifi wifi = db.wifi.Find(id);
            if (wifi == null)
            {
                return HttpNotFound();
            }
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", wifi.nodeID);
            return View(wifi);
        }

        // POST: wifis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "wifiID,IP,SSID,power,nodeID")] wifi wifi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wifi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", wifi.nodeID);
            return View(wifi);
        }

        // GET: wifis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wifi wifi = db.wifi.Find(id);
            if (wifi == null)
            {
                return HttpNotFound();
            }
            return View(wifi);
        }

        // POST: wifis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            wifi wifi = db.wifi.Find(id);
            db.wifi.Remove(wifi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
