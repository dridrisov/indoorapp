﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Indoor_app.Models;
using System.Configuration;
using System.Web.Configuration;

namespace Indoor_app.Controllers
{
    [Authorize(Users = AccessLevelUsers.Users)]
    public class buildingsController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: buildings
        [AllowAnonymous]
        public ActionResult Index(string sortOrder)
        {
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.NameEngSortParm = sortOrder == "Name" ? "name_desc" : "Name";

            var buildings = from s in db.buildings select s;
            switch (sortOrder)
            {
                case "Name":
                    buildings = buildings.OrderBy(s => s.buildingName);
                    break;
                case "name_desc":
                    buildings = buildings.OrderByDescending(s => s.buildingName);
                    break;
                case "id_desc":
                    buildings = buildings.OrderByDescending(s => s.buildingID);
                    break;
                default:
                    buildings = buildings.OrderBy(s => s.buildingID);
                    break;
            }
            return View(buildings.ToList());
        }

        // GET: buildings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            buildings buildings = db.buildings.Find(id);
            if (buildings == null)
            {
                return HttpNotFound();
            }
            return View(buildings);
        }

        // GET: buildings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: buildings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "buildingID,buildingName,buildingNameRu,matrixFile,imageFile,description,latt,longt,city,street,house,country,compass,imageFilePath")] buildings building)
        {
            if (ModelState.IsValid)
            {
                string fileName = building.buildingName;
                string extension = Path.GetExtension(building.imageFilePath.FileName);
                fileName = fileName + extension;

                string directoryName = building.buildingName + "\\";
                directoryName = Path.Combine(Server.MapPath("~/maps/"), directoryName);

                building.imageFile = "~/maps/" + building.buildingName + "/" + fileName;

                fileName = Path.Combine(directoryName, fileName);

                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                building.imageFilePath.SaveAs(fileName);

                db.buildings.Add(building);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(building);
        }

        // GET: buildings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            buildings buildings = db.buildings.Find(id);
            if (buildings == null)
            {
                return HttpNotFound();
            }
            return View(buildings);
        }

        // POST: buildings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "buildingID,buildingName,buildingNameRu,matrixFile,imageFile,description,latt,longt,city,street,house,country,compass,imageFilePath")] buildings building)
        {
            if (ModelState.IsValid)
            {
                buildings oldBuilding = db.buildings.AsNoTracking().Where(b => b.buildingID == building.buildingID).SingleOrDefault();
                string oldBuildingName = oldBuilding.buildingName;
                string oldDirectoryName = Path.Combine(Server.MapPath("~/maps/"), oldBuildingName);

                db.Entry(building).State = EntityState.Modified;

                //сначала проверяем загружали ли новую фотографию
                //если да, то удаляем старую и обновляем поле новой записью
                //если нет, то написано
                if (building.imageFilePath != null)
                {
                    string oldFileName = oldBuildingName + "/" + oldBuildingName + Path.GetExtension(oldBuilding.imageFile);
                    oldFileName = Path.Combine(Server.MapPath("~/maps/"), oldFileName);

                    if (System.IO.File.Exists(oldFileName))
                    {
                        System.IO.File.Delete(oldFileName);
                    }


                    string newFileName = building.buildingName;
                    string extension = Path.GetExtension(building.imageFilePath.FileName);
                    newFileName = newFileName + extension;

                    building.imageFile = "~/maps/" + oldBuildingName + "/" + newFileName;

                    newFileName = Path.Combine(oldDirectoryName, newFileName);

                    building.imageFilePath.SaveAs(newFileName);
                }
                else
                {
                    //нужно чтобы когда не меняем картинку не записывалась пустое значение
                    //другой варик настроить для POST передачу старых данных картинки
                    db.Entry(building).Property(x => x.imageFile).IsModified = false;
                }

                db.SaveChanges();

                string directoryName = building.buildingName;
                directoryName = Path.Combine(Server.MapPath("~/maps/"), directoryName);

                //название папки и фотографии здания являются названиями этого здания
                //поэтому необходимо изменить их при изменении названия здания
                if (!Directory.Exists(oldDirectoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
                else if (Directory.Exists(oldDirectoryName))
                {
                    if (oldDirectoryName != directoryName)
                    {
                        Directory.Move(oldDirectoryName, directoryName);

                        if (oldBuilding.imageFile != null)
                        {
                            string oldFileName = building.buildingName + "/" + oldBuildingName + Path.GetExtension(oldBuilding.imageFile);
                            string newFileName = building.buildingName + "/" + building.buildingName + Path.GetExtension(oldBuilding.imageFile);

                            building.imageFile = "~/maps/" + building.buildingName + "/" + building.buildingName + Path.GetExtension(oldBuilding.imageFile);

                            newFileName = Path.Combine(Server.MapPath("~/maps/"), newFileName);
                            oldFileName = Path.Combine(Server.MapPath("~/maps/"), oldFileName);

                            if (!System.IO.File.Exists(newFileName))
                            {
                                System.IO.File.Move(oldFileName, newFileName);
                            }

                            db.SaveChanges();
                        }
                    }
                };

                return RedirectToAction("Index");
            }
            return View(building);
        }

        // GET: buildings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            buildings buildings = db.buildings.Find(id);
            if (buildings == null)
            {
                return HttpNotFound();
            }
            return View(buildings);
        }

        // POST: buildings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            buildings buildings = db.buildings.Find(id);

            string directoryName = buildings.buildingName;
            directoryName = Path.Combine(Server.MapPath("~/maps/"), directoryName);

            db.buildings.Remove(buildings);
            db.SaveChanges();

            if (Directory.Exists(directoryName))
            {
                Directory.Delete(directoryName, true);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
