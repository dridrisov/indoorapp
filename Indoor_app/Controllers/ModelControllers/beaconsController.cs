﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Indoor_app.Models;

namespace Indoor_app.Controllers
{
    [Authorize(Users = AccessLevelUsers.Users)]
    public class beaconsController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: beacons
        [AllowAnonymous]
        public ActionResult Index(string sortOrder)
        {
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.BatteryLevelSortParm = sortOrder == "batteryLevel" ? "batteryLevel_desc" : "batteryLevel";
            ViewBag.NodeIdSortParm = sortOrder == "nodeID" ? "nodeID_desc" : "nodeID";
            ViewBag.MapLevelSortParm = sortOrder == "mapLevel" ? "mapLevel_desc" : "mapLevel";
            ViewBag.BuildingSortParm = sortOrder == "building" ? "building_desc" : "building";

            var beacons = from s in db.beacons select s;
            switch (sortOrder)
            {
                case "batteryLevel":
                    beacons = beacons.OrderBy(s => s.batteryLevel);
                    break;
                case "batteryLevel_desc":
                    beacons = beacons.OrderByDescending(s => s.batteryLevel);
                    break;
                case "nodeID":
                    beacons = beacons.OrderBy(s => s.nodes.nodeID);
                    break;
                case "nodeID_desc":
                    beacons = beacons.OrderByDescending(s => s.nodes.nodeID);
                    break;
                case "mapLevel":
                    beacons = beacons.OrderBy(s => s.nodes.floors.mapLevel);
                    break;
                case "mapLevel_desc":
                    beacons = beacons.OrderByDescending(s => s.nodes.floors.mapLevel);
                    break;
                case "building":
                    beacons = beacons.OrderBy(s => s.nodes.floors.buildings.buildingName);
                    break;
                case "building_desc":
                    beacons = beacons.OrderByDescending(s => s.nodes.floors.buildings.buildingName);
                    break;
                case "id_desc":
                    beacons = beacons.OrderByDescending(s => s.beaconID);
                    break;
                default:
                    beacons = beacons.OrderBy(s => s.beaconID);
                    break;
            }
            return View(beacons.ToList());
            //var beacons = db.beacons.Include(b => b.nodes);
            //return View(beacons.ToList());
        }

        // GET: beacons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            beacons beacons = db.beacons.Find(id);
            if (beacons == null)
            {
                return HttpNotFound();
            }
            return View(beacons);
        }

        // GET: beacons/Create
        public ActionResult Create()
        {
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID");
            return View();
        }

        // POST: beacons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "beaconID,title,uuid,minor,major,batteryLevel,language,nodeID")] beacons beacons)
        {
            if (ModelState.IsValid)
            {
                db.beacons.Add(beacons);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", beacons.nodeID);
            return View(beacons);
        }

        // GET: beacons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            beacons beacons = db.beacons.Find(id);
            if (beacons == null)
            {
                return HttpNotFound();
            }
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", beacons.nodeID);
            return View(beacons);
        }

        // POST: beacons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "beaconID,title,uuid,minor,major,batteryLevel,language,nodeID")] beacons beacons)
        {
            if (ModelState.IsValid)
            {
                db.Entry(beacons).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.nodeID = new SelectList(db.nodes, "nodeID", "nodeID", beacons.nodeID);
            return View(beacons);
        }

        // GET: beacons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            beacons beacons = db.beacons.Find(id);
            if (beacons == null)
            {
                return HttpNotFound();
            }
            return View(beacons);
        }

        // POST: beacons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            beacons beacons = db.beacons.Find(id);
            db.beacons.Remove(beacons);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
