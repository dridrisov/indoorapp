﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Indoor_app.Models;

namespace Indoor_app.Controllers
{
    [Authorize(Users = AccessLevelUsers.Users)]
    public class nodesController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: nodes
        [AllowAnonymous]
        public ActionResult Index(string sortOrder)
        {
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.RoomTypeSortParm = sortOrder == "roomType" ? "roomType_desc" : "roomType";
            ViewBag.RoomNumSortParm = sortOrder == "roomNum" ? "roomNum_desc" : "roomNum";
            ViewBag.TypeSortParm = sortOrder == "type" ? "type_desc" : "type";
            ViewBag.FloorIdSortParm = sortOrder == "floorID" ? "floorID_desc" : "floorID";
            ViewBag.MapLevelSortParm = sortOrder == "mapLevel" ? "mapLevel_desc" : "mapLevel";
            ViewBag.BuildingSortParm = sortOrder == "building" ? "building_desc" : "building";

            var nodes = from s in db.nodes select s;
            switch (sortOrder)
            {
                case "roomType":
                    nodes = nodes.OrderBy(s => s.roomType);
                    break;
                case "roomType_desc":
                    nodes = nodes.OrderByDescending(s => s.roomType);
                    break;
                case "roomNum":
                    nodes = nodes.OrderBy(s => s.roomNum);
                    break;
                case "roomNum_desc":
                    nodes = nodes.OrderByDescending(s => s.roomNum);
                    break;
                case "type":
                    nodes = nodes.OrderBy(s => s.type);
                    break;
                case "type_desc":
                    nodes = nodes.OrderByDescending(s => s.type);
                    break;
                case "floorID":
                    nodes = nodes.OrderBy(s => s.floors.floorID);
                    break;
                case "floorID_desc":
                    nodes = nodes.OrderByDescending(s => s.floors.floorID);
                    break;
                case "mapLevel":
                    nodes = nodes.OrderBy(s => s.floors.mapLevel);
                    break;
                case "mapLevel_desc":
                    nodes = nodes.OrderByDescending(s => s.floors.mapLevel);
                    break;
                case "building":
                    nodes = nodes.OrderBy(s => s.floors.buildings.buildingName);
                    break;
                case "building_desc":
                    nodes = nodes.OrderByDescending(s => s.floors.buildings.buildingName);
                    break;
                case "id_desc":
                    nodes = nodes.OrderByDescending(s => s.nodeID);
                    break;
                default:
                    nodes = nodes.OrderBy(s => s.nodeID);
                    break;
            }
            return View(nodes.ToList());

            //var nodes = db.nodes.Include(n => n.floors);
            //return View(nodes.ToList());
        }

        // GET: nodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            nodes nodes = db.nodes.Find(id);
            if (nodes == null)
            {
                return HttpNotFound();
            }
            return View(nodes);
        }

        // GET: nodes/Create
        public ActionResult Create()
        {
            ViewBag.floorID = new SelectList(db.floors, "floorID", "floorID");
            return View();
        }

        // POST: nodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "nodeID,description,roomType,roomNum,coordX,coordY,type,floorID")] nodes nodes)
        {
            if (ModelState.IsValid)
            {
                db.nodes.Add(nodes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.floorID = new SelectList(db.floors, "floorID", "floorID", nodes.floorID);
            return View(nodes);
        }

        // GET: nodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            nodes nodes = db.nodes.Find(id);

            if (nodes == null)
            {
                return HttpNotFound();
            }
            ViewBag.floorID = new SelectList(db.floors, "floorID", "floorID", nodes.floorID);
            
            
            //ViewBag.buildingNameRUS = new SelectList(db.buildings, "buildingNameRU", "buildingNameRU", nodes.floors.buildings.buildingNameRu);
            return View(nodes);
        }

        // POST: nodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "nodeID,description,roomType,roomNum,coordX,coordY,type,floorID")] nodes nodes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nodes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.floorID = new SelectList(db.floors, "floorID", "floorID", nodes.floorID);

            //мультиселект?
            //@Html.DropDownList("buildingNameRUS", null, htmlAttributes: new { @class = "form-control" })
            //ViewBag.buildingNameRUS = new SelectList(db.buildings, "floorID", "buildingNameRU", nodes.floors.buildings.buildingNameRu);

            return View(nodes);
        }

        // GET: nodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            nodes nodes = db.nodes.Find(id);
            if (nodes == null)
            {
                return HttpNotFound();
            }
            return View(nodes);
        }

        // POST: nodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            nodes nodes = db.nodes.Find(id);
            db.nodes.Remove(nodes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
