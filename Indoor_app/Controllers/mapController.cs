﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Indoor_app.Models;
using Indoor_app.Models.DTO;

namespace Indoor_app.Controllers
{
    [System.Web.Mvc.Authorize]
    public class mapController : Controller
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        // GET: map
        public async Task<ActionResult> Index()
        {
            var buildings = await db.buildings.Include(b => b.floors).Select(b =>
                new buildingDetailsDTO()
                {
                    buildingID = b.buildingID,
                    buildingName = b.buildingName,
                    buildingNameRu = b.buildingNameRu,
                    matrixFile = b.matrixFile,
                    floors = b.floors.Select(floor => new floorDTO()
                    {
                        floorID = floor.floorID,
                        mapLevel = floor.mapLevel ?? 0,
                        buildingID = floor.buildingID ?? 0
                    }).ToList(),
                }).ToListAsync();


            var view = new MapViewModel()
            {
                BuildingId = buildings.FirstOrDefault().buildingID,
                Buildings = buildings,
                Floors = new List<floorDTO>()
            };

            return View(view);
        }

        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> Index(int buildingId)
        {
            var buildings = await db.buildings.Include(b => b.floors).Select(b =>
                new buildingDetailsDTO()
                {
                    buildingID = b.buildingID,
                    buildingName = b.buildingName,
                    buildingNameRu = b.buildingNameRu,
                    matrixFile = b.matrixFile,
                    floors = b.floors.Select(floor => new floorDTO()
                    {
                        floorID = floor.floorID,
                        mapLevel = floor.mapLevel ?? 0,
                        buildingID = floor.buildingID ?? 0
                    }).ToList(),
                }).ToListAsync();

            var floors = buildings.Where(x => x.buildingID == buildingId).Select(x => x.floors).FirstOrDefault();

            var view = new MapViewModel()
            {
                BuildingId = buildingId,
                Buildings = buildings,
                Floors = floors
            };

            return View(view);
        }
    }

    public class MapViewModel
    {
        public int BuildingId { get; set; }
        public List<buildingDetailsDTO> Buildings { get; set; }
        public List<floorDTO> Floors { get; set; }
    }
}