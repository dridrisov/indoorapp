﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Indoor_app.Models;
using Indoor_app.Models.DTO;

namespace Indoor_app.Controllers
{
    [RoutePrefix("api")]
    public class floorsApiController : ApiController
    {
        private BeaconDBEntities db = new BeaconDBEntities();            

        [HttpGet, Route("floorFullDetails/{id:int}", Name = "=getAllFloorDetailsByID")]
        public async System.Threading.Tasks.Task<IHttpActionResult> floorFullDetails(int id)
        {
            var floor = await db.floors.Include(b => b.nodes).Select(b => new floorDetailsDTO()
            {
                floorID = b.floorID,
                description = b.description,
                directory = b.directory,
                mapLevel = b.mapLevel ?? 0,
                width = b.width ?? 0,
                height = b.height ?? 0,
                buildingID = b.buildingID ?? 0,
                nodes = b.nodes.Select(node => new nodeDTO()
                {
                    nodeID = node.nodeID,
                    description = node.description,
                    roomNum = node.roomNum,
                    roomType = node.roomType,
                    coordX = node.coordX ?? 0,
                    coordY = node.coordY ?? 0,
                    type = node.type,
                    floorID = node.floorID ?? 0,
                    mapLevel = b.mapLevel ?? 0,
                    buildingID = b.buildingID ?? 0,
                    beacon = node.beacons.Select(beacon => new beaconDTO()
                    {
                        beaconID = beacon.beaconID,
                        title = beacon.title,
                        uuid = beacon.uuid,
                        minor = beacon.minor ?? 0,
                        major = beacon.major ?? 0,
                        batteryLevel = beacon.batteryLevel ?? 0,
                        language = beacon.language,
                        nodeID = beacon.nodeID ?? 0
                    }).FirstOrDefault(),
                    wifiSpot = node.wifi.Select(wifiSpot => new wifiDTO()
                    {
                        wifiID = wifiSpot.wifiID,
                        IP = wifiSpot.IP,
                        SSID = wifiSpot.SSID,
                        power = wifiSpot.power ?? 0,
                        nodeID = wifiSpot.nodeID ?? 0
                    }).FirstOrDefault()
                }).ToList(),
            }).SingleOrDefaultAsync(b => b.floorID == id);


            if (floor == null)
            {
                return NotFound();
            }

            return Ok(floor);
        }

        [HttpPost, Route("addFloorDetails", Name = "addFloorDetails"), ResponseType(typeof(int))]
        public IHttpActionResult addfloordetails(floorDetailsDTO floorDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.buildings.Count(b => b.buildingID == floorDetails.floorID) <= 0)
            {
                return BadRequest("referenced buildingID doesnt exist");
            }

            floors floor = new floors()
            {
                description = floorDetails.description,
                directory = floorDetails.directory,
                mapLevel = floorDetails.mapLevel,
                width = floorDetails.width,
                height = floorDetails.height,
                buildingID = floorDetails.buildingID
            };

            db.floors.Add(floor);
            db.SaveChanges();

            foreach (var node in floorDetails.nodes)
            {
                nodes new_node = new nodes()
                {
                    description = node.description,
                    roomNum = node.roomNum,
                    roomType = node.roomType,
                    coordX = node.coordX,
                    coordY = node.coordY,
                    type = node.type,
                    floorID = floor.floorID
                };

                db.nodes.Add(new_node);
                db.SaveChanges();

                if (node.type == "beacon")
                {
                    db.beacons.Add(new beacons()
                    {
                        title = node.beacon.title,
                        uuid = node.beacon.uuid,
                        minor = node.beacon.minor,
                        major = node.beacon.major,
                        batteryLevel = node.beacon.batteryLevel,
                        language = node.beacon.language,
                        nodeID = new_node.nodeID
                    });
                }
                else if (node.type == "wifi")
                {
                    db.wifi.Add(new wifi()
                    {
                        IP = node.wifiSpot.IP,
                        SSID = node.wifiSpot.SSID,
                        power = node.wifiSpot.power,
                        nodeID = new_node.nodeID
                    });
                }
            }

            db.SaveChanges();

            return Ok(floor.floorID);
        }
    }
}
