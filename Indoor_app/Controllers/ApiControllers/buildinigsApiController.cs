﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Indoor_app.Models;
using Indoor_app.Models.DTO;


namespace Indoor_app.Controllers
{
    [RoutePrefix("api")]
    public class buildinigsApiController : ApiController
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        [HttpGet, Route("allBuildings", Name = "getAllBuildings")]
        public IQueryable<buildingDTO> allbuildings()
        {
            return db.buildings.Select(b =>
                new buildingDTO()
                {
                    buildingID = b.buildingID,
                    buildingName = b.buildingName,
                    buildingNameRu = b.buildingNameRu,
                    description = b.description,
                });
        }

        [HttpGet, Route("buildingDetails/{id:int}", Name = "getBuildingDetailsByID")]
        public async System.Threading.Tasks.Task<IHttpActionResult> buildingDetails(int id)
        {
            var building = await db.buildings.Include(b => b.floors).Select(b =>
                new buildingDetailsDTO()
                {
                    buildingID = b.buildingID,
                    buildingName = b.buildingName,
                    buildingNameRu = b.buildingNameRu,
                    matrixFile = b.matrixFile,
                    floors = b.floors.Select(floor => new floorDTO()
                    {
                        floorID = floor.floorID,
                        mapLevel = floor.mapLevel ?? 0,
                        buildingID = floor.buildingID ?? 0
                    }).ToList(),
                    imageFile = b.imageFile,
                    description = b.description,
                    latt = b.latt ?? 0,
                    longt = b.longt ?? 0,
                    city = b.city,
                    street = b.street,
                    house = b.house,
                    country = b.country,
                    compass = b.compass
                }).SingleOrDefaultAsync(b => b.buildingID == id);
            if (building == null)
            {
                return NotFound();
            }

            return Ok(building);
        }

        [HttpGet, Route("matrix/{id:int}", Name = "getMatrixForBuilding")]
        public IHttpActionResult matrixForBuilding(int id)
        {
            buildings building = db.buildings.Find(id);
            if (building == null)
            {
                return NotFound();
            }

            return Ok(building.matrixFile);
        }

        [HttpPost, Route("updateMatrix/{id:int}", Name = "updateMatrixForBuilding"), ResponseType(typeof(int))]
        public IHttpActionResult updateMatrix(int id, [FromBody]MatrixDto matrixdto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            buildings building_ = db.buildings.Find(id);
            if (building_ == null)
            {
                return NotFound();
            }

            building_.matrixFile = matrixdto.matrix;
            
            db.Entry(building_).Property(x => x.matrixFile).IsModified = true;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (db.buildings.Count(e => e.buildingID == id) <= 0)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(building_.buildingID);
        }
    }
    public class MatrixDto
    {
        public string matrix { get; set; }
    }
}
