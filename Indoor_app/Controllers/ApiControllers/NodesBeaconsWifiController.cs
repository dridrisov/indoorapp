﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Indoor_app.Models;
using Indoor_app.Models.DTO;

namespace Indoor_app.Controllers.ApiControllers
{
    [RoutePrefix("api")]
    public class NodesBeaconsWifiController : ApiController
    {
        private BeaconDBEntities db = new BeaconDBEntities();

        //lambda expressions for DTO object for Select() method
        private static readonly Expression<Func<beacons, beaconDTO>> AsBeaconDTO =
            b => new beaconDTO
            {
                beaconID = b.beaconID,
                title = b.title,
                uuid = b.uuid,
                minor = b.minor ?? 0,
                major = b.major ?? 0,
                batteryLevel = b.batteryLevel ?? 0,
                language = b.language,
                nodeID = b.nodeID ?? 0
            };

        private static readonly Expression<Func<wifi, wifiDTO>> AswifiDTO =
            wifiSpot => new wifiDTO()
            {
                wifiID = wifiSpot.wifiID,
                IP = wifiSpot.IP,
                SSID = wifiSpot.SSID,
                power = wifiSpot.power ?? 0,
                nodeID = wifiSpot.nodeID ?? 0
            };

        //WEB API methods

        //методы для нод
        [HttpGet, Route("nodeByID/{id:int}", Name = "nodeByID")]
        public async System.Threading.Tasks.Task<IHttpActionResult> nodeByID(int id)
        {
            var node_ = await db.nodes.Include(b => b.beacons).
                                       Include(b => b.wifi).
                                       Select(node => new nodeDTO()
                                       {
                                           nodeID = node.nodeID,
                                           description = node.description,
                                           roomNum = node.roomNum,
                                           roomType = node.roomType,
                                           coordX = node.coordY ?? 0,
                                           coordY = node.coordY ?? 0,
                                           type = node.type,
                                           floorID = node.floorID ?? 0,
                                           beacon = node.beacons.Select(beacon => new beaconDTO()
                                           {
                                               beaconID = beacon.beaconID,
                                               title = beacon.title,
                                               uuid = beacon.uuid,
                                               minor = beacon.minor ?? 0,
                                               major = beacon.major ?? 0,
                                               batteryLevel = beacon.batteryLevel ?? 0,
                                               language = beacon.language,
                                               nodeID = beacon.nodeID ?? 0
                                           }).FirstOrDefault(),
                                           wifiSpot = node.wifi.Select(wifiSpot => new wifiDTO()
                                           {
                                               wifiID = wifiSpot.wifiID,
                                               IP = wifiSpot.IP,
                                               SSID = wifiSpot.SSID,
                                               power = wifiSpot.power ?? 0,
                                               nodeID = wifiSpot.nodeID ?? 0
                                           }).FirstOrDefault()
                                       }).SingleOrDefaultAsync(b => b.nodeID == id);
            if (node_ == null)
            {
                return NotFound();
            }
            return Ok(node_);
        }

        [HttpPost, Route("addNode", Name = "addNode"), ResponseType(typeof(int))]
        public IHttpActionResult addNode(nodeDTO node)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var floor = db.floors.FirstOrDefault(b => b.buildingID == node.buildingID && b.mapLevel == node.mapLevel);

            if (floor == null)
            {
                return BadRequest("referenced floor doesnt exist");
            }

            nodes newNode = new nodes()
            {
                description = node.description,
                roomNum = node.roomNum,
                roomType = node.roomType,
                coordX = node.coordX,
                coordY = node.coordY,
                type = node.type,
                floorID = floor.floorID
            };

            db.nodes.Add(newNode);
            db.SaveChanges();

            if (node.type == "beacon" && node.beacon != null)
            {
                db.beacons.Add(new beacons()
                {
                    title = node.beacon.title,
                    uuid = node.beacon.uuid,
                    minor = node.beacon.minor,
                    major = node.beacon.major,
                    batteryLevel = node.beacon.batteryLevel,
                    language = node.beacon.language,
                    nodeID = newNode.nodeID
                });
            }
            if (node.type == "wifi" && node.wifiSpot != null)
            {
                db.wifi.Add(new wifi()
                {
                    IP = node.wifiSpot.IP,
                    SSID = node.wifiSpot.SSID,
                    power = node.wifiSpot.power,
                    nodeID = newNode.nodeID
                });
            }

            db.SaveChanges();

            return Ok(newNode.nodeID);
        }

        //[HttpPut, Route("updateNode/{id:int}", Name = "updateNode"), ResponseType(typeof(int))]
        [HttpPost, Route("updateNode/{id:int}", Name = "updateNode"), ResponseType(typeof(int))]
        public IHttpActionResult updateNode(int id, nodeDTO node)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != node.nodeID)
            {
                return BadRequest();
            }

            var floor = db.floors.AsNoTracking().FirstOrDefault(b => b.buildingID == node.buildingID && b.mapLevel == node.mapLevel);

            nodes newNode = new nodes()
            {
                nodeID = node.nodeID,
                description = node.description,
                roomNum = node.roomNum,
                roomType = node.roomType,
                coordX = node.coordX,
                coordY = node.coordY,
                type = node.type,
                floorID = floor.floorID
            };
            beacons beacon = new beacons();
            wifi wifiSpot = new wifi();

            db.Entry(newNode).State = EntityState.Modified;

            if (node.type == "beacon" && node.beacon != null)
            {
                beacon.beaconID = node.beacon.beaconID;
                beacon.title = node.beacon.title;
                beacon.uuid = node.beacon.uuid;
                beacon.minor = node.beacon.minor;
                beacon.major = node.beacon.major;
                beacon.batteryLevel = node.beacon.batteryLevel;
                beacon.language = node.beacon.language;
                beacon.nodeID = newNode.nodeID; //оставить newNode?

                db.Entry(beacon).State = EntityState.Modified;
            }
            if (node.type == "wifi" && node.wifiSpot != null)
            {
                wifiSpot.wifiID = node.wifiSpot.wifiID;
                wifiSpot.IP = node.wifiSpot.IP;
                wifiSpot.SSID = node.wifiSpot.SSID;
                wifiSpot.power = node.wifiSpot.power;
                wifiSpot.nodeID = newNode.nodeID;

                db.Entry(wifiSpot).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (db.nodes.Count(e => e.nodeID == id) <= 0)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(newNode.nodeID);
        }

        //[HttpDelete, Route("deleteNode/{id:int}", Name = "deleteNode"), ResponseType(typeof(int))]
        [HttpPost, Route("deleteNode/{id:int}", Name = "deleteNode"), ResponseType(typeof(int))]
        public IHttpActionResult deleteNode(int id)
        {
            nodes node = db.nodes.Find(id);
            if (node == null)
            {
                return NotFound();
            }

            if (node.beacons.Count() > 0)
            {
                foreach (var beacon in node.beacons.ToArray()) //???
                {
                    db.beacons.Remove(beacon);
                    db.SaveChanges();
                }
            }
            if (node.wifi.Count() > 0)
            {
                foreach (var wifiSpot in node.wifi.ToArray())
                {
                    db.wifi.Remove(wifiSpot);
                    db.SaveChanges();
                }
            }

            db.nodes.Remove(node);
            db.SaveChanges();


            return Ok(node.nodeID);
        }

        [HttpGet, Route("allNodesByBuilding/{id:int}", Name = "getAllNodesByBuilding")]
        public NodesMatrixDto allnodesbybuilding(int id)
        {
            List<nodeDTO> nodeDTOs = new List<nodeDTO>();

            List<int> floorIDs = db.floors.Where(f => f.buildingID == id).Select(f => f.floorID).ToList();

            var matrix = db.buildings.Find(id).matrixFile;

            foreach (var floorid in floorIDs)
            {
                List<nodeDTO> nodes = db.nodes.Where(n => n.floorID.Value == floorid).Select(node => new nodeDTO()
                {
                    nodeID = node.nodeID,
                    description = node.description,
                    roomNum = node.roomNum,
                    roomType = node.roomType,
                    coordX = node.coordX ?? 0,
                    coordY = node.coordY ?? 0,
                    type = node.type,
                    floorID = node.floorID ?? 0,
                    mapLevel = db.floors.FirstOrDefault(x => x.floorID == node.floorID).mapLevel.Value,
                    beacon = node.beacons.Select(beacon => new beaconDTO()
                    {
                        beaconID = beacon.beaconID,
                        title = beacon.title,
                        uuid = beacon.uuid,
                        minor = beacon.minor ?? 0,
                        major = beacon.major ?? 0,
                        batteryLevel = beacon.batteryLevel ?? 0,
                        language = beacon.language,
                        nodeID = beacon.nodeID ?? 0
                    }).FirstOrDefault(),
                    wifiSpot = node.wifi.Select(wifiSpot => new wifiDTO()
                    {
                        wifiID = wifiSpot.wifiID,
                        IP = wifiSpot.IP,
                        SSID = wifiSpot.SSID,
                        power = wifiSpot.power ?? 0,
                        nodeID = wifiSpot.nodeID ?? 0
                    }).FirstOrDefault()
                }).ToList<nodeDTO>();

                nodeDTOs.AddRange(nodes);
            }
            return new NodesMatrixDto()
            {
                Nodes = nodeDTOs,
                Matrix = matrix
            };
        }

        public class NodesMatrixDto
        {
            public List<nodeDTO> Nodes { get; set; }
            public string Matrix { get; set; }
        }

        [HttpGet, Route("allNodes", Name = "getAllNodes")]
        public IQueryable<nodeDTO> allnodes()
        {
            return db.nodes.Select(node => new nodeDTO()
            {
                nodeID = node.nodeID,
                description = node.description,
                roomNum = node.roomNum,
                roomType = node.roomType,
                coordX = node.coordX ?? 0,
                coordY = node.coordY ?? 0,
                type = node.type,
                floorID = node.floorID ?? 0,
                beacon = node.beacons.Select(beacon => new beaconDTO()
                {
                    beaconID = beacon.beaconID,
                    title = beacon.title,
                    uuid = beacon.uuid,
                    minor = beacon.minor ?? 0,
                    major = beacon.major ?? 0,
                    batteryLevel = beacon.batteryLevel ?? 0,
                    language = beacon.language,
                    nodeID = beacon.nodeID ?? 0
                }).FirstOrDefault(),
                wifiSpot = node.wifi.Select(wifiSpot => new wifiDTO()
                {
                    wifiID = wifiSpot.wifiID,
                    IP = wifiSpot.IP,
                    SSID = wifiSpot.SSID,
                    power = wifiSpot.power ?? 0,
                    nodeID = wifiSpot.nodeID ?? 0
                }).FirstOrDefault()
            });
        }

        //методы для биконов
        [HttpGet, Route("beaconByParameters/{UUID}/{minor:int}/{major:int}", Name = "getBeaconByParameters")]
        public IHttpActionResult beaconByParameters(string UUID, int minor, int major)
        {
            var beacon_ = db.beacons.Select(AsBeaconDTO).SingleOrDefault(b => (b.uuid.Equals(UUID, StringComparison.OrdinalIgnoreCase)) && (b.minor == minor) && (b.major == major));

            if (beacon_ == null)
            {
                return NotFound();
            }
            return Ok(beacon_.nodeID);

            var node_ = db.nodes.Include(b => b.beacons).
                                       Select(node => new nodeDTO()
                                       {
                                           nodeID = node.nodeID,
                                           description = node.description,
                                           roomNum = node.roomNum,
                                           roomType = node.roomType,
                                           coordX = node.coordY ?? 0,
                                           coordY = node.coordY ?? 0,
                                           type = node.type,
                                           floorID = node.floorID ?? 0,
                                           beacon = node.beacons.Select(beacon => new beaconDTO()
                                           {
                                               beaconID = beacon.beaconID,
                                               title = beacon.title,
                                               uuid = beacon.uuid,
                                               minor = beacon.minor ?? 0,
                                               major = beacon.major ?? 0,
                                               batteryLevel = beacon.batteryLevel ?? 0,
                                               language = beacon.language,
                                               nodeID = beacon.nodeID ?? 0
                                           }).FirstOrDefault(),
                                           wifiSpot = null
                                       }).SingleOrDefaultAsync(b => b.nodeID == beacon_.nodeID);
            if (node_ == null)
            {
                return NotFound();
            }

            return Ok(node_);
        }

        [HttpGet, Route("floorID/{id:int}", Name = "getFloorIDByBeaconID")]
        public IHttpActionResult floorIDbyBeaconID(int id)
        {
            beacons beacon = db.beacons.Find(id);

            if (beacon == null)
            {
                return NotFound();
            }

            nodes node = db.nodes.Find(beacon.nodeID);

            if(node == null)
            {
                return NotFound();
            }

            return Ok(node.floorID);
        }

        [HttpGet, Route("buildingID/{id:int}", Name = "getBuildingIDByBeaconID")]
        public IHttpActionResult buildingIDbyBeaconID(int id)
        {
            beacons beacon = db.beacons.Find(id);

            if (beacon == null)
            {
                return NotFound();
            }

            nodes node = db.nodes.Find(beacon.nodeID);

            if (node == null)
            {
                return NotFound();
            }

            floors floor = db.floors.Find(node.floorID);

            if (floor == null)
            {
                return NotFound();
            }

            return Ok(floor.buildingID);
        }
    }  
}
