﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Indoor_app.Startup))]
namespace Indoor_app
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
