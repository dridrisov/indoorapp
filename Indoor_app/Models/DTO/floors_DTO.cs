﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indoor_app.Models.DTO
{
    public class floorInstanceDTO
    {
        public int floorID { get; set; }
        public int mapLevel { get; set; }
        public string description { get; set; }
        public string directory { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int buildingID { get; set; }
    }
    public class floorDTO
    {
        public int floorID { get; set; }
        public int mapLevel { get; set; }
        public int buildingID { get; set; }

    }

    public class floorDetailsDTO
    {
        public int floorID { get; set; }
        public string description { get; set; }
        public string directory { get; set; }
        public int mapLevel { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int buildingID { get; set; }    
        public List<nodeDTO> nodes { get; set; }
    }
}