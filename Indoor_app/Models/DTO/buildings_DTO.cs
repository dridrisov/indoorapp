﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indoor_app.Models.DTO
{
    public class buildingDTO
    {
        public int buildingID { get; set; }
        public string buildingName { get; set; }
        public string buildingNameRu { get; set; }
        public string description { get; set; }
    }
    public class buildingDetailsDTO
    {
        public int buildingID { get; set; }
        public string buildingName { get; set; }
        public string buildingNameRu { get; set; }
        public string matrixFile { get; set; }
        public string imageFile { get; set; }
        public string description { get; set; }
        public List<floorDTO> floors { get; set; }
        public double latt { get; set; }
        public double longt { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house { get; set; }
        public string country { get; set; }
        public string compass { get; set; }
    }
}