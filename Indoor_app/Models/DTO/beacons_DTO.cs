﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indoor_app.Models.DTO
{
    public class beaconDTO
    {
        public int beaconID { get; set; }
        public string title { get; set; }
        public string uuid { get; set; }
        public int minor { get; set; }
        public int major { get; set; }
        public double batteryLevel { get; set; }
        public string language { get; set; }
        public int nodeID { get; set; }
    }
}