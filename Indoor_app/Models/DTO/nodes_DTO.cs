﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indoor_app.Models.DTO
{
    public class nodeDTO
    {
        public int nodeID { get; set; }
        public string description { get; set; }
        public string roomNum { get; set; }
        public string roomType { get; set; }
        public int coordX { get; set; }
        public int coordY { get; set; }
        public string type { get; set; }
        public int floorID { get; set; }
        public int buildingID { get; set; } //раньше не было
        public int mapLevel { get; set; } //раньше не было
        public beaconDTO beacon { get; set; }
        public wifiDTO wifiSpot { get; set; }
    }
}