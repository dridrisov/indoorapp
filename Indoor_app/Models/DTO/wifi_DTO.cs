﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indoor_app.Models.DTO
{
    public class wifiDTO
    {
        public int wifiID { get; set; }
        public string IP { get; set; }
        public string SSID { get; set; }
        public double power { get; set; }
        public int nodeID { get; set; }
    }
}